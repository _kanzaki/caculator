part of 'calculator_bloc.dart';

class CalculatorEvent extends Equatable {
  const CalculatorEvent();

  @override
  List<Object?> get props => [];
}

class NumClickEvent extends CalculatorEvent {
  const NumClickEvent(this.input);

  final String input;

  @override
  List<Object> get props => [input];
}

class ValueUpdate extends CalculatorEvent {
  const ValueUpdate(this.type);

  final String type;

  @override
  List<Object> get props => [type];
}

class BackSpace extends CalculatorEvent{
  const BackSpace();
}

class Clear extends CalculatorEvent{
  const Clear();
}
