part of 'calculator_bloc.dart';

class CalculatorState extends Equatable {
  const CalculatorState({
    this.buffer = '', //前一個輸入的數字
    this.currentNum = '', //實際計算的數字
    this.showNum = '', //顯示在畫面上的數字
    this.mode = '', //當前計算模式
  });

  final String buffer;
  final String currentNum;
  final String showNum;
  final String mode;

  CalculatorState copyWith({String? buffer, String? currentNum,String? showNum, String? mode,}) {
    return CalculatorState(
      buffer: buffer ?? this.buffer,
      currentNum: currentNum ?? this.currentNum,
      showNum: showNum ?? this.showNum,
      mode: mode ?? this.mode,
    );
  }

  @override
  List<Object> get props => [buffer, currentNum, showNum, mode];
}
