import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:format/format.dart';

part 'calculator_event.dart';

part 'calculator_state.dart';

class CalculatorBloc extends Bloc<CalculatorEvent, CalculatorState> {
  CalculatorBloc() : super(CalculatorState()) {
    on<NumClickEvent>((event, emit) => _numInput(event, emit));
    on<ValueUpdate>((event, emit) => _valueUpdate(event, emit));
    on<BackSpace>((event, emit) => _backSpace(emit));
    on<Clear>((event, emit) => emit(
        state.copyWith(buffer: '', currentNum: '', showNum: '', mode: '')));
  }

  //數字輸入
  void _numInput(NumClickEvent event, Emitter<CalculatorState> emit) {
    String _temp = '';

    //顯示計算結果後沒繼續按計算功能而直接按數字，則先清空原本的數字
    if (state.mode == '=' && state.currentNum.isNotEmpty)
      emit(state.copyWith(currentNum: '', mode: ''));

    if (event.input == '.') {
      _temp = state.currentNum +
          (state.currentNum.contains('.') ? '' : event.input); //不重複輸入小數點
    } else {
      _temp = state.currentNum + event.input;
    }

    if (_temp.length > 1 && !_temp.contains('.') && _temp[0] == '0') {
      _temp = _temp.replaceFirst('0', ''); //處理0開頭數字
    }
    emit(state.copyWith(currentNum: _temp, showNum: _numberFormat(_temp)));
  }

  //刪除最後一個字
  void _backSpace(Emitter<CalculatorState> emit) {
    String _temp = '';
    if (state.currentNum.isNotEmpty) {
      _temp = state.currentNum.substring(0, state.currentNum.length - 1);
    }
    emit(state.copyWith(currentNum: _temp, showNum: _temp));
  }

  //數字根據計算模式更新
  void _valueUpdate(ValueUpdate event, Emitter<CalculatorState> emit) {
    if (event.type == '=') {
      String _equalResult = calculate(state.mode);
      if (state.buffer.isEmpty)
        emit(state.copyWith(
            currentNum: state.currentNum,
            showNum: _numberFormat(state.currentNum))); //
      else {
        emit(state.copyWith(
          buffer: '',
          currentNum: _equalResult,
          showNum: _numberFormat(_equalResult),
          mode: '=',
        ));
      }
    } else if (event.type == '+/-') {
      String _reverseResult = calculate('+/-');
      emit(state.copyWith(
          currentNum: _reverseResult, showNum: _numberFormat(_reverseResult)));
    } else {
      if (state.buffer.isEmpty) {
        emit(state.copyWith(
            buffer: state.currentNum,
            currentNum: '',
            showNum: _numberFormat(state.currentNum)));
      } else {
        String _calcResult = calculate(state.mode);
        emit(state.copyWith(
            buffer: _calcResult,
            currentNum: '',
            showNum: _numberFormat(_calcResult)));
      }
      emit(state.copyWith(mode: event.type == '=' ? '' : event.type));
    }
  }

  //處理計算
  String calculate(String operator) {
    double value1 = 0;
    double value2 = 0;
    double result = 0;
    if (state.buffer.isNotEmpty) value1 = double.parse(state.buffer);
    if (state.currentNum.isNotEmpty) value2 = double.parse(state.currentNum);
    switch (operator) {
      case '+':
        result = value1 + value2;
        break;
      case '-':
        result = value1 - value2;
        break;
      case '*':
        result = value1 * value2;
        break;
      case '+/-':
        result = -1 * value2;
        break;
      case '/':
        if (value2 == 0)
          result = 0;
        else
          result = value1 / value2;
        break;
    }
    print('${value1} ${operator} ${value2} = ${result}');
    return result.toString();
  }

  //格式轉換
  String _numberFormat(String value) {
    String _reformat = '';
    if (value.isEmpty) return _reformat;
    if (value.contains('.') && value.split('.').length > 1) {
      String _intPart = format('{:,d}', int.parse(value.split('.')[0]));
      String _decimalPart = value.split('.')[1];
      String _temp = _intPart + '.' + _decimalPart;

      _reformat = _temp.length > 15 ? _temp.substring(0, 14) : _temp; //小數後太多位的話就直接切除
    } else if (value.length > 12) {
      _reformat = format('{:.2g}', int.parse(value.split('.')[0]));
    } else {
      _reformat = format('{:,d}', int.parse(value.split('.')[0]));
    }
    return _reformat;
  }
}
