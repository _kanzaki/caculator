import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../component/button.dart';
import 'bloc/calculator_bloc.dart';

class CalculatorPage extends StatefulWidget {
  @override
  _CalculatorPageState createState() => _CalculatorPageState();
}

class _CalculatorPageState extends State<CalculatorPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Container(
              padding: EdgeInsets.only(right: 10),
              child: BlocBuilder<CalculatorBloc, CalculatorState>(
                buildWhen: (previous,current)=>previous.showNum!=current.showNum,
                  builder: (context, state) {
                return Text(
                  state.showNum,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 40,
                  ),
                );
              }),
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                CalcButton(
                  title: 'AC',
                  color: ButtonColor.white,
                  onPressed: () {
                    context.read<CalculatorBloc>().add(Clear());
                  },
                ),
                CalcButton(
                  title: '+/-',
                  color: ButtonColor.white,
                  onPressed: () {
                    context.read<CalculatorBloc>().add(ValueUpdate('+/-'));
                  },
                ),
                CalcButton(
                  title: '<',
                  color: ButtonColor.white,
                  onPressed: () {
                    context.read<CalculatorBloc>().add(BackSpace());
                  },
                ),
                CalcButton(
                  title: '+',
                  color: ButtonColor.yellow,
                  onPressed: () {
                    context.read<CalculatorBloc>().add(ValueUpdate('+'));
                  },
                ),
              ],
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                CalcButton(
                  title: '7',
                  onPressed: () {
                    context.read<CalculatorBloc>().add(NumClickEvent('7'));
                  },
                ),
                CalcButton(
                  title: '8',
                  onPressed: () {
                    context.read<CalculatorBloc>().add(NumClickEvent('8'));
                  },
                ),
                CalcButton(
                  title: '9',
                  onPressed: () {
                    context.read<CalculatorBloc>().add(NumClickEvent('9'));
                  },
                ),
                CalcButton(
                  title: '-',
                  color: ButtonColor.yellow,
                  onPressed: () {
                    context.read<CalculatorBloc>().add(ValueUpdate('-'));
                  },
                ),
              ],
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                CalcButton(
                  title: '4',
                  onPressed: () {
                    context.read<CalculatorBloc>().add(NumClickEvent('4'));
                  },
                ),
                CalcButton(
                  title: '5',
                  onPressed: () {
                    context.read<CalculatorBloc>().add(NumClickEvent('5'));
                  },
                ),
                CalcButton(
                  title: '6',
                  onPressed: () {
                    context.read<CalculatorBloc>().add(NumClickEvent('6'));
                  },
                ),
                CalcButton(
                  title: 'x',
                  color: ButtonColor.yellow,
                  onPressed: () {
                    context.read<CalculatorBloc>().add(ValueUpdate('*'));
                  },
                ),
              ],
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                CalcButton(
                  title: '1',
                  onPressed: () {
                    context.read<CalculatorBloc>().add(NumClickEvent('1'));
                  },
                ),
                CalcButton(
                  title: '2',
                  onPressed: () {
                    context.read<CalculatorBloc>().add(NumClickEvent('2'));
                  },
                ),
                CalcButton(
                  title: '3',
                  onPressed: () {
                    context.read<CalculatorBloc>().add(NumClickEvent('3'));
                  },
                ),
                CalcButton(
                  title: '÷',
                  color: ButtonColor.yellow,
                  onPressed: () {
                    context.read<CalculatorBloc>().add(ValueUpdate('/'));
                  },
                ),
              ],
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                CalcButton(
                  title: '0',
                  wideButton: true,
                  onPressed: () {
                    context.read<CalculatorBloc>().add(NumClickEvent('0'));
                  },
                ),
                CalcButton(
                  title: '.',
                  onPressed: () {
                    context.read<CalculatorBloc>().add(NumClickEvent('.'));
                  },
                ),
                CalcButton(
                  title: '=',
                  color: ButtonColor.yellow,
                  wideButton: false,
                  onPressed: () {
                    context.read<CalculatorBloc>().add(ValueUpdate('='));
                  },
                ),
              ],
            ),

          ],
        ));
  }

}
