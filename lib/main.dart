import 'package:caculator/page/bloc/calculator_bloc.dart';
import 'package:caculator/page/calculatoe_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

void main() {
  runApp(const MyApp());
}

//a6a6a6

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(360, 690),
      builder: () {
        return MaterialApp(
          title: 'Calculator Demo',
          home: Scaffold(
            appBar: AppBar(title: Text('Calculator Demo'), centerTitle: true,),
            body: BlocProvider(
              create: (context) => CalculatorBloc(),
              child: CalculatorPage(),
            ),
            backgroundColor: Colors.black,
          ),
        );
      }
    );
  }
}
