import 'package:flutter/material.dart';

double buttonSize = 60;
enum ButtonColor { normal, white, yellow } //按鈕樣式

class CalcButton extends StatelessWidget {
  CalcButton({
    required this.title,
    required this.onPressed,
    this.color = ButtonColor.normal,
    this.wideButton = false,
  });

  final String title;
  final VoidCallback onPressed;
  final ButtonColor color;
  final bool wideButton;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(200)),
        clipBehavior: Clip.hardEdge,
        child: ElevatedButton(
          onPressed: onPressed,
          style: ElevatedButton.styleFrom(
              primary: _buttonColorSwitcher(color),
              padding: EdgeInsets.zero),
          child: Container(
            width: wideButton ? 2 * buttonSize + 16 : 1 * buttonSize,
            height: buttonSize,
            alignment: Alignment.center,
            child: Text(
              title,
              style: TextStyle(fontSize: 30,
                  color: color!=ButtonColor.normal ? Color(0xff333333) : Colors.white),
            ),
          ),
        ),
      ),
    );
  }

  //按鈕樣式對應顏色
  Color _buttonColorSwitcher(ButtonColor type) {
    switch (type) {
      case ButtonColor.normal:
        return Color(0xff333333);
      case ButtonColor.white:
        return Color(0xffacacac);
      case ButtonColor.yellow:
        return Color(0xfffb9502);
    }
  }
}
